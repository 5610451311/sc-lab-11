package Ex03;

public class Main 
{
	public static void main(String[] args){
		Refrigerator freez = new Refrigerator(6);
		try{
			System.out.println("First : "+freez);
			
			//add item in refrigerator
			freez.put("Juice");
			freez.put("Cake");
			freez.put("Brownie");
			
			System.out.println("Before take out Cake : "+freez);
			//take out cake
			freez.takeOut("Cake");
			
			System.out.println("After take out Cake : "+freez);

			//add item in refrigerator
			freez.put("Milk");
			freez.put("Yogurt");
			freez.put("Curry");
			freez.put("Bread");
			
			System.out.println("Now in refrigerator have : "+freez);
			
			freez.takeOut("Yogurt");
			System.out.println("After take out Yogurt : "+freez);
			
			freez.takeOut("Cheese");
			
			freez.put("Cheese");
			System.out.println("After put Cheese : "+freez);
			
			// Now full Refrigerator 
			//Test put item but cannot put in
			freez.put("Cheese stick");
			
			
		}
		catch(FullException e){
			System.err.println(e.getMessage());
		}
	}


	
}

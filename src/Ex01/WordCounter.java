package Ex01;
import java.util.HashMap;


public class WordCounter 
{
	private String message;
	private HashMap<String,Integer> map;
	
	public WordCounter(String message)
	{
		this.message = message;
		map = new HashMap<String,Integer>();
	}
	
	
	public void count()
	{
		for(String str : message.split(" "))
		{
			if(!map.containsKey(str))
			{
				map.put(str,1);
			}
			else
			{
				int a = map.get(str);
				map.replace(str, a, ++a);
			}
		}
	}
	
	
	public int hasWord(String word)
	{
		if(map.containsKey(word))
		{
			return map.get(word);
		}
		else
		{
			return 0;
		}
	}
	
}
